//
//  ContentView.swift
//  ScrollToReload
//
//  Created by Jakob Fiegerl on 22.03.21.
//

import SwiftUI

struct ContentView: View {
    
    @State var showProgressView = false
    
    var body: some View {
        ScrollView {
            VStack {
                ProgressView()
                    .opacity(showProgressView ? 1 : 0)
                    .padding(.top, -20)
                    .padding(.all, showProgressView ? 20 : 0)
                
                Text("Hello World")
            }
        }
        .gesture(
            DragGesture().onChanged { value in
                if value.translation.height > 10 {
                    showProgressView = true
                    triggerReload()
                }
            }
        )
    }
    
    func triggerReload() {
        print("Network call started")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            showProgressView = false
            print("Network call ended")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
