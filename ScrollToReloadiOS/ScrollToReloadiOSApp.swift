//
//  ScrollToReloadiOSApp.swift
//  ScrollToReloadiOS
//
//  Created by Jakob Fiegerl on 22.03.21.
//

import SwiftUI

@main
struct ScrollToReloadiOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
